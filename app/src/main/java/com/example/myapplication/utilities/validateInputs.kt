package com.example.myapplication.utilities

import android.text.TextUtils
import android.util.Patterns
import java.util.regex.Matcher
import java.util.regex.Pattern


object ValidateInputs {
    private const val blockCharacters = "[$&+~;=\\\\?@|/'<>^*()%!-]"

    //*********** Validate Email Address ********//
    fun isValidEmail(email: String?): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    //*********** Validate Phone Number********//
    fun isValidPhoneNo(phoneNo: String?): Boolean {
        return !TextUtils.isEmpty(phoneNo) && Patterns.PHONE.matcher(phoneNo).matches()
    }

    //*********** Validate Number Input ********//
    fun isValidNumber(number: String): Boolean {
        val regExpn = "^[0-9]{1,24}$"
        val inputStr: CharSequence = number
        val pattern: Pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE)
        val matcher: Matcher = pattern.matcher(inputStr)
        return matcher.matches()
    }
}

