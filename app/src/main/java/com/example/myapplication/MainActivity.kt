package com.example.myapplication

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.View.OnClickListener
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.network.ApiClient
import com.example.myapplication.network.ApiInterface
import com.example.myapplication.utilities.ValidateInputs
import com.example.myapplication.utilities.tools
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.net.URI
import java.net.URISyntaxException


open class MainActivity : AppCompatActivity(), OnClickListener{

     var name :String = ""
     var number :String = ""
     var address :String = ""
     var email :String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initializeview()
    }

    private fun initializeview() {
        var ed_name = findViewById<TextInputEditText>(R.id.ed_name)
        var et_phone = findViewById<TextInputEditText>(R.id.et_phone)
        var et_address = findViewById<TextInputEditText>(R.id.et_address)
        var et_email = findViewById<TextInputEditText>(R.id.et_email)
        var tv_file = findViewById<TextView>(R.id.tv_file)
        var img_file = findViewById<ImageView>(R.id.img_file)
        var btn_submit = findViewById<MaterialButton>(R.id.btn_submit)

        btn_submit.setOnClickListener(this)
        img_file.setOnClickListener(this)

    }

    override fun onClick(view: View?) {
        when(view?.id){
           R.id.btn_submit -> {
               if (tools().isNetworkAvailable(this)) {
                   if(validation()){
                       SubmitData()
                   }
               } else {
                   Toast.makeText(this,"Please Check Your Network connection",Toast.LENGTH_LONG).show()
               }
           }
            R.id.img_file ->{
                getDocument()
            }
       }
    }

    private fun validation(): Boolean {
        var failFlag = true
        if( !TextUtils.isEmpty(ed_name.getText().toString().trim()))
        {
            name =ed_name.text.toString()
        }else{
            failFlag = false;
            ed_name.setError( "A value is required" );
        }

        if(ValidateInputs.isValidPhoneNo(et_phone.text.toString().trim()))
        {
            number =et_phone.text.toString()
        }else{
            failFlag = false;
            et_phone.setError( "A value is required" );
        }

        if(ValidateInputs.isValidMail(et_email.text.toString()))
        {
            email =et_phone.text.toString()
        }else{
            failFlag = false;
            et_email.setError( "A value is required" );
        }

        if(!TextUtils.isEmpty(et_address.text.toString().trim()))
        {
            address =et_address.text.toString()
        }else{
            failFlag = false;
            et_address.setError( "A value is required" );
        }

        return failFlag
    }

    private fun SubmitData() {
        val apiService: ApiInterface = ApiClient.client!!.create(ApiInterface::class.java)

        val call: Call<ResponseBody?>? = apiService!!.list1(name,number,address,email)

        call!!.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>?,
                response: Response<ResponseBody?>?
            ) {
                try {
                    if (response!!.code() == 200) {
                        val stringResponse = response!!.body()!!.string()
                    }
                } catch (e: Exception) {
                    Log.d("Error Message", "" + e.message)
                } finally {
                }
            }

            override fun onFailure(
                call: Call<ResponseBody?>?,
                t: Throwable?
            ) {
            }
        })

    }

    private fun getDocument() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "application/pdf"
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        // Only the system receives the ACTION_OPEN_DOCUMENT, so no need to test.
        startActivityForResult(intent, 101)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 101 && resultCode == Activity.RESULT_OK && null != data) {
            val uri = data.data
            val uriString = uri.toString()
            val myFile = File(uriString)
            val path = myFile.absolutePath
            var displayName: String? = null
            if (uriString.startsWith("content://")) {
                var cursor: Cursor? = null
                try {
                    cursor = this.contentResolver.query(uri!!, null, null, null, null)
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName =
                            cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                            tv_file.setText(displayName)
                    }
                } finally {
                    cursor!!.close()
                }
            } else if (uriString.startsWith("file://")) {
                displayName = myFile.name
                tv_file.setText(displayName)
            }
        }
    }
}