package com.example.myapplication.network

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface ApiInterface {

//    @FormUrlEncoded
    @GET(Constants.LIST1)
    fun list1(
    @Query("name") name: String?,
    @Query("number") number: String?,
    @Query("address") address: String?,
    @Query("email") email: String?
    ): Call<ResponseBody?>?

}